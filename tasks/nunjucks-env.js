'use strict';

var nunjucks = require('nunjucks');

var env;

exports.configure = function (paths) {
    env = nunjucks.configure( paths.templates.templatesDir );
}

exports.getEnv = function () {
    return env;
}