'use strict';
var connect = require('gulp-connect');
var cors = require('cors');

module.exports = function (gulp, paths) {
    return function () {
        connect.server({
            port: process.env.PORT || 3000,
            livereload: true,
            root: paths.dest,
            middleware: function() {
                return [cors()];
            }
        });
    };
};
