'use strict';

var fs = require('fs');
var path = require('path');
var requireNew = require('require-new');
var rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');

function writeManifest (paths) {
    return rev.manifest( paths.manifest, {
        base: paths.dest,
        merge: true
    });
}

function loadManifest (paths) {
    var manifest;
    try {
        manifest = requireNew( paths.manifest );
    }
    catch (e) {
        manifest = {};
    }

    return manifest;
}

function replacePaths (gulp, paths, manifestStream) {
    manifestStream = manifestStream || gulp.src(paths.manifest);
    return revReplace({
        manifest: manifestStream,
        prefix: paths.publicPath
    });
}

exports.fingerprint = rev;
exports.load = loadManifest;
exports.replace = replacePaths;
exports.write = writeManifest;
