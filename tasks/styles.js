'use strict';
var autoprefixer = require('autoprefixer-core');
var csswring = require('csswring');
var errorHandler = require('./error-handler');
var less = require('gulp-less');
var manifest = require('./manifest');
var mqpacker = require('css-mqpacker');
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');

exports.dev = function (gulp, paths) {
    return function () {
        return gulp.src(paths.styles.src, { base: paths.src })
            .pipe( sourcemaps.init() )
            .pipe( plumber({ errorHandler: errorHandler }) )
            .pipe( less({
                paths: paths.styles.includePaths
            }) )
            .on('error', errorHandler)
            .pipe( postcss([
                autoprefixer({
                    browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'IE 9'],
                    cascade: false
                })
            ]) )
            .pipe( manifest.replace(gulp, paths) )
            .pipe( manifest.fingerprint() )
            .pipe( sourcemaps.write('.') )
            .pipe( gulp.dest(paths.dest) )
            .pipe( manifest.write(paths) )
            .pipe( gulp.dest(paths.dest) )
            ;
    };
};

exports.dist = function (gulp, paths) {
    return function () {
        return gulp.src(paths.styles.src, { base: paths.src })
            .pipe( plumber({ errorHandler: errorHandler }) )
            .pipe( less({
                paths: paths.styles.includePaths
            }) )
            .on('error', errorHandler)
            .pipe( postcss([
                autoprefixer({
                    browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'IE 9'],
                    cascade: false
                }),
                mqpacker,
                csswring
            ]) )
            .pipe( manifest.replace(gulp, paths) )
            .pipe( manifest.fingerprint() )
            .pipe( gulp.dest(paths.dest) )
            .pipe( manifest.write(paths) )
            .pipe( gulp.dest(paths.dest) )
            ;
    };
};