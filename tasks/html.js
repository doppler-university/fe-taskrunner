'use strict';

var connect = require('gulp-connect');
var errorHandler = require('./error-handler');
var nunjucks = require('gulp-render-nunjucks');
var env = require('./nunjucks-env');
var manifest = require('./manifest');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');

module.exports = function (gulp, paths) {

    env.configure(paths);

    return function () {
        return gulp.src(paths.templates.src, { base: paths.templates.base })
            .pipe( plumber({ errorHandler: errorHandler }) )
            .pipe( nunjucks.render() )
            .on('error', errorHandler)
            // Rename the file so it becomes {file.name}/index.html
            // eg. pages/foo.html -> foo/index.html
            .pipe( rename({ suffix: '/index', extname: '.html'}) )
            .pipe( manifest.replace(gulp, paths) )
            .pipe( gulp.dest(paths.dest) )
            .pipe( connect.reload() )
            ;
    };

};
