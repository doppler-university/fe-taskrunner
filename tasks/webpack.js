'use strict';

var errorHandler = require('./error-handler');
var fs = require('fs');
var manifest = require('./manifest');
var path = require('path');
var plumber = require('gulp-plumber');
var webpack =require('gulp-webpack-build');
var wp = require('webpack');


function onRun (paths) {

    return function(err, stats) {
        var assets = stats.toJson().assetsByChunkName;
        var webpackManifest = Object.keys(assets)
            .reduce(function (memo, key) {
                var value, relativePath, relativeKey, relativeVal;
                value = assets[key];
                if( Array.isArray(value) ){
                    value = value[0];
                }
                relativePath = path.relative(paths.dest, path.join(paths.dest, 'js'));
                relativeKey = path.join(relativePath, key + path.extname(value));
                relativeVal = path.join(relativePath, value);
                memo[relativeKey] = relativeVal;
                return memo;
            },
            manifest.load(paths)
        );
        var jsonStr = JSON.stringify(webpackManifest, null, 4);
        fs.writeFileSync(paths.manifest, jsonStr);
    };
}


exports.dev = function (gulp, paths) {

    var config = {
        useMemoryFs: true,
        progress: true
    };

    var props = {
        devtool: 'sourcemap',
        debug: true,
        output: {
            path: path.join(paths.dest, '/js'),
            publicPath: 'js',
            filename: '[name]-[chunkhash].js'
        }
    };

    return function() {
        return gulp.src(paths.scripts.src, { base: paths.src })
            .pipe( plumber({ errorHandler: errorHandler }) )
            .pipe( webpack.init(config) )
            .pipe( webpack.props(props) )
            .pipe( webpack.run(onRun(paths)) )
            .pipe( webpack.format({ version: false, timings: true }) )
            .pipe( webpack.failAfter({ errors: true, warnings: true }) )
            .on('error', errorHandler)
            .pipe(gulp.dest(paths.dest))
        ;
    }
};

exports.dist = function (gulp, paths) {
    var config = {
        useMemoryFs: true,
        progress: true
    };

    var props = {
        devtool: 'none',
        debug: false,
        output: {
            path: path.join(paths.dest, '/js'),
            publicPath: 'js',
            filename: '[name]-[chunkhash].js'
        },
        plugins: [
            new wp.DefinePlugin({
                'process.env': {
                    // This has effect on the react lib size
                    'NODE_ENV': JSON.stringify('production')
                }
            }),
            new wp.optimize.DedupePlugin(),
            new wp.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                }
            }),
            // Ensure hash of chunk ids are consistent - without this plugin the hash of
            // the same file contents may change between builds.
            new wp.optimize.OccurenceOrderPlugin(true)
        ]
    };

    return function() {
        return gulp.src(paths.scripts.src, { base: paths.src })
            .pipe( plumber({ errorHandler: errorHandler }) )
            .pipe( webpack.init(config) )
            .pipe( webpack.props(props) )
            .pipe( webpack.run(onRun(paths)) )
            .pipe( webpack.format({ version: false, timings: true }) )
            .pipe( webpack.failAfter({ errors: true, warnings: true }) )
            .on('error', errorHandler)
            .pipe(gulp.dest(paths.dest))
            ;
    }
};
