'use strict';

var manifest = require('./manifest');

module.exports = function (gulp, paths) {
    return function () {
        return gulp.src(paths.static.src, { base: paths.src })
            .pipe( manifest.fingerprint() )
            .pipe( gulp.dest(paths.dest) )
            .pipe( manifest.write(paths) )
            .pipe( gulp.dest(paths.dest) )
            ;
    };
};
