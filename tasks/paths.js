'use strict';

var path = require('path');

// Use full/absolute path names for paths, so that gulp-rev
// does not write the absolute path to the manifest.
module.exports = {

    src: path.join(process.cwd(), '/src'),

    // Build path
    dest: path.join(process.cwd(), '/build'),

    // Distribution path
    dist: path.join(process.cwd(), '/dist'),

    publicPath: '/',

    manifest: path.join(process.cwd(), '/build/manifest.json'),

    scripts: {
        src: path.join(process.cwd(), '/src/webpack.config.js'),
        watch: path.join(process.cwd(), '/src/js/**/*')
    },

    styles: {
        src: [
            path.join(process.cwd(), '/src/styles/main.less'),
            path.join(process.cwd(), '/src/styles/pages/**/*.less'),
        ],
        includePaths: [
            // Include bootstrap from node modules.
            // Needed so the individual modules in bootstrap/less can resolve dependencies to one another.
            path.join(process.cwd(), '/node_modules/bootstrap/less'),
            path.join(process.cwd(), '/node_modules')
        ],
        watch: path.join(process.cwd(), '/src/styles/**/*.less')
    },

    static: {
        src: [
            path.join(process.cwd(), '/src/images/**/*.{jpg,png,webp}'),
            path.join(process.cwd(), '/src/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}'),
            path.join(process.cwd(), '/src/videos/**/*.{mp4,m4v,webm,ogg}')
        ],
        watch: path.join(process.cwd(), '/src/{images,fonts,videos}/**/*')
    },

    svg: {
        src: path.join(process.cwd(), '/src/svg/**/*.svg'),
        watch: path.join(process.cwd(), '/src/svg/**/*.svg')
    },

    templates: {
        src: path.join(process.cwd(), '/src/templates/pages/**/*.{html,nunj,nunjucks}'),
        base: path.join(process.cwd(), '/src/templates/pages'),
        templatesDir: path.join(process.cwd(), '/src/templates'),
        watch: path.join(process.cwd(), '/src/templates/**/*.{html,nunj,nunjucks}')
    }

};
