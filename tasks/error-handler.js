var notify = require('gulp-notify');
var path = require('path');



module.exports = function errorHandler (error) {
    var message = error.message.replace(process.cwd(), '.');

    notify({
        title: 'Gulp Task Error',
        message: message
    }).write(error);

    console.log(error.toString());

    this.emit('end');
};
