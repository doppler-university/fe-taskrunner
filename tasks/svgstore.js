var cheerio = require('gulp-cheerio'),
    manifest = require('./manifest'),
    rename = require('gulp-rename'),
    svgmin = require('gulp-svgmin'),
    svgstore = require('gulp-svgstore')
    ;

module.exports = function (gulp, paths) {
    return function () {
        return gulp.src(paths.svg.src, { base: paths.src })
            .pipe( svgmin({
                plugins: [
                    // Turn this off, so that inline styles can be used to specify colours
                    { convertStyleToAttrs: false }
                ]
            }) )
            .pipe( cheerio({
                run: function ($) {
                    $('[fill]').removeAttr('fill');
                    $('[stroke]').removeAttr('stroke');
                },
                parserOptions: { xmlMode: true }
            }) )
            .pipe( svgstore() )
            .pipe( rename('svg/sprite.svg') )
            .pipe( manifest.fingerprint() )
            .pipe( gulp.dest(paths.dest) )
            .pipe( manifest.write(paths) )
            .pipe( gulp.dest(paths.dest) )
            ;
    };
};
