'use strict';

exports.clean = require('./clean');
exports.html = require('./html');
exports.errorHandler = require('./error-handler');
exports.manifest = require('./manifest');
exports.nunjucksEnv = require('./nunjucks-env');
exports.paths = require('./paths');
exports.server = require('./server');
exports.static = require('./static');
exports.styles = require('./styles');
exports.svgstore = require('./svgstore');
exports.webpack = require('./webpack');
