'use strict';

var del = require('del');
var path = require('path');

var opts = {force: true};


exports.all = function (paths) {
    return function (done) {
        del([paths.dest], opts, done);
    };
};

exports.build = function (paths) {
    return function (done) {
        del([
            '!' + path.join(paths.dest, '/images/'),
            '!' + path.join(paths.dest, '/images/**'),
            path.join(paths.dest, '/**/*')
        ], opts, done);
    };
};

exports.dist = function (paths) {
    return function (done) {
        del([
            paths.dist,
            path.join(paths.dist, '**/*.map')
        ], opts, done)
    }
};

exports.html = function (paths) {
    return function (done) {
        del( path.join(paths.dest, '/**/*.html'), opts, done);
    };
};

exports.js = function (paths) {
    return function (done) {
        del([path.join(paths.dest, 'js')], opts, done);
    };
};

exports.static = function (paths) {
    return function (done) {
        del(paths.static, opts, done);
    };
};

exports.styles = function (paths) {
    return function (done) {
        del([
            path.join(paths.dest, 'styles')
        ], opts, done);
    };
};

exports.svg = function (paths) {
    return function (done) {
        del([
            path.join(paths.dest, '/svg')
        ], opts, done);
    };
};
