'use strict';

var path = require('path');
var defaultTasks = require('./tasks');
var defaultPaths = defaultTasks.paths;

module.exports = function (gulp, tasks, paths) {

    if (!tasks) {
        tasks = defaultTasks;
    }

    if (!paths) {
        paths = tasks.paths || defaultPaths;
    }

    var runSequence = require('run-sequence').use(gulp);

    // ========================================================
    //  Cleanup tasks
    // ========================================================
    gulp.task('clean:all', tasks.clean.all(paths));
    gulp.task('clean:build', tasks.clean.build(paths));
    gulp.task('clean:dist', tasks.clean.dist(paths));
    gulp.task('clean:js', tasks.clean.js(paths));
    gulp.task('clean:static', tasks.clean.static(paths) );
    gulp.task('clean:styles', tasks.clean.styles(paths));
    gulp.task('clean:svg', tasks.clean.svg(paths));


    // ========================================================
    //  Tasks to process assets
    // ========================================================
    gulp.task('styles', ['clean:styles'], tasks.styles.dev(gulp, paths));
    gulp.task('styles:dist', tasks.styles.dist(gulp, paths));
    gulp.task('static', tasks.static(gulp, paths));
    gulp.task('svgstore', tasks.svgstore(gulp, paths));
    gulp.task('webpack', [], tasks.webpack.dev(gulp, paths));
    gulp.task('webpack:dist', tasks.webpack.dist(gulp, paths));

    gulp.task('copy:dist', ['clean:dist'], function(){
        return gulp.src(path.join(paths.dest, '/**/*'))
            .pipe(gulp.dest(paths.dist))
        ;
    });




    // ========================================================
    //  Workflow for watch and rebuild tasks
    // ========================================================
    gulp.task('watch', function () {
        gulp.watch(paths.styles.watch, ['rebuild:styles']);
        gulp.watch(paths.static.watch, ['rebuild:static']);
        gulp.watch(paths.scripts.watch, ['rebuild:webpack']);
        gulp.watch(paths.svg.watch, ['rebuild:svgstore']);
    });

    gulp.task('rebuild:styles', function (done) {
        runSequence('styles', done);
    });

    gulp.task('rebuild:static', ['clean:static'], function (done) {
        runSequence('static', 'build', done);
    });

    gulp.task('rebuild:svgstore', ['clean:svg'], function (done) {
        runSequence('svgstore', 'webpack', done);
    });

    gulp.task('rebuild:webpack', function (done) {
        runSequence('webpack', done);
    });



    // ========================================================
    //  Main workflow tasks
    // ========================================================

    gulp.task('server', tasks.server(gulp, paths));

    // Quick build - does not process images
    gulp.task('build', [], function (done) {
        runSequence(
            'static',
            'svgstore',
            'styles',
            'webpack',
            done
        );
    });

    // Deprecated
    gulp.task('build:all', ['clean:all'], function (done) {

        runSequence(
            'build',
            ['server', 'watch'],
            done
        );
    });

    gulp.task('build:dist', ['clean:all'], function (done) {
        runSequence(
            ['static', 'svgstore'],
            ['styles:dist', 'webpack:dist'],
            ['copy:dist'],
            done
        );
    });

    gulp.task('default', ['build', 'server', 'watch']);

};
